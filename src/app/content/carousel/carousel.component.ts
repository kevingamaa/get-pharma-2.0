import { Component, OnInit, Input } from '@angular/core';
import { HelperService } from 'src/app/service/helper.service';

@Component({
    selector: 'app-carousel',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
    public slideConfig = {
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: false,
        infinite: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 930,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 764,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '100px',
                    infinite: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '40px',
                    infinite: false
                }
            }
        ]
    };

    public slides = [
        {
            img: './assets/fotos/noticia.png',
            title: 'TEXTO 1',
            description: 'TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO',
            data: '01-08-12 '
        },
        {
            img: './assets/fotos/noticia.png',
            title: 'TEXTO 1',
            description: 'TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO',
            data: '01-08-12 '
        },
        {
            img: './assets/fotos/noticia.png',
            title: 'TEXTO 1',
            description: 'TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO',
            data: '01-08-12 '
        },
        {
            img: './assets/fotos/noticia.png',
            title: 'TEXTO 1',
            description: 'TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO',
            data: '01-08-12 '
        },
    ];
    constructor(public helper: HelperService) { }

    ngOnInit() {

    }

    slickInit(e) {
    }

    breakpoint(e) {
    }

    afterChange(e) {
    }

    beforeChange(e) {
    }


}
