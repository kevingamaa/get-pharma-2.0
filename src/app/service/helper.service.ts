import { Injectable } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Injectable({
    providedIn: 'root'
})
export class HelperService {
    public isMobile: boolean;

    constructor(private breakpointObserver: BreakpointObserver) {
        this.breakpointObserver.observe(['(min-width: 767px)'])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    this.isMobile = false;
                } else {
                    this.isMobile = true;
                }
            });
    }


    public scroll(id, effect = true): void {
        const el = document.getElementById(id);
        if (effect) {
            el.scrollIntoView({
                behavior: 'smooth'
            });
        } else {
            el.scrollIntoView();
        }
    }
}
