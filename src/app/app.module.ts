import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { LayoutModule } from '@angular/cdk/layout';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { NavComponent } from './nav/nav.component';
import { ContentComponent } from './content/content.component';
import { CarouselModule } from 'ngx-carousels';
import { CarouselComponent } from './content/carousel/carousel.component';
import { HelperService } from './service/helper.service';
import { ModalComponent } from './nav/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavComponent,
    ContentComponent,
    CarouselComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    CarouselModule,
    SlickCarouselModule
  ],
  providers: [HelperService],
  bootstrap: [AppComponent]
})
export class AppModule { }
