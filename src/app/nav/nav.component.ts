import { Component, OnInit, HostListener } from '@angular/core';
import { HelperService } from '../service/helper.service';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
    public opened: boolean;
    public navTop: boolean;
    constructor(public helper: HelperService) { }

    ngOnInit() {
    }
    @HostListener('window:scroll', ['$event'])
    onWindowScroll( event) {
        if ( window.pageYOffset > 400 ) {
            this.navTop = true;
        } else {
            this.navTop = false;
        }

    }

    toggleNav(effect) {
        if (effect) {
            if (this.opened) {
                this.opened = false;
            } else {
                this.opened = true;
            }
        } else {
            this.opened = false;
        }
    }
}
